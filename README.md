# README #

My name is Mandisa Dlubom a web developer graduate from CodeSpace Academy.

### What is this repository for? ###

This repository is for Future Media coding challenge whereby I am required to create a website using MEAN stack.


###Developer Challenge Overview###

To develop a simple web application with basic user information using MEAN stack.

###Process###

The primary coding language will be JavaScript - it is used on the server-side as well (node.js). The stack is called MEAN for MongoDB, Express.js Angular.js and Node.js. Have to use these technologies together with HTML and CSS.


The website has the following:

Landing page 

-with a short looping video background that scales optimally based on screen size
-Sign in and sign up links
-Sign in block opens on this page
-Sign up (also on landing page)
Captures the following: firstname, surname, email, favourite color (choose from drop down or add new), password (needs to be at least 5 characters long), profile picture
-Top navbar (show only once logged in)
-Links to ‘sign out’, my profile, home

My Profile

-Basically the same fields as sign up except password (you can update these details)
-Password reset action (sends an email from server side with a link that can be followed

Home

-List of last logins by the current user (essentially a report)
-Ability to page and filter this report (server-side paging)



